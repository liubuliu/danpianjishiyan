#include "INIC.h"
void LEDInit(void)
{
	
    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<16); 
    LPC_IOCON->PIO2_0 &= ~0x07;    
    LPC_IOCON->PIO2_0 |= 0x00; 
    LPC_IOCON->PIO2_1 &= ~0x07;   
    LPC_IOCON->PIO2_1 |= 0x00; 
    LPC_SYSCON->SYSAHBCLKCTRL &= ~(1<<16); 
    
    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<6);  
  
    LPC_GPIO2->DIR |= (1<<0); 
    LPC_GPIO2->DATA |= (1<<0); 
	
    LPC_GPIO2->DIR |= (1<<1); 
    LPC_GPIO2->DATA |= (1<<1); 
}

void Delayms(uint16_t ms)
{
	SysTick->CTRL &= ~(1<<2);
	SysTick->LOAD = SystemCoreClock /2/1000*ms-1;
	SysTick->VAL  = 0;
  SysTick->CTRL |= ((1<<1) | (1<<0));
	while(! ticks);
	ticks =0;
	SysTick ->CTRL =0;

}



void SysTick_Handler(void)
{
	ticks ++;
}


